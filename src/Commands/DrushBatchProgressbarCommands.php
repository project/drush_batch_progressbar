<?php

namespace Drupal\drush_batch_progressbar\Commands;

use Consolidation\OutputFormatters\StructuredData\UnstructuredListData;
use Drush\Commands\DrushCommands;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class DrushBatchProgressbarCommands extends DrushCommands {

  /**
   * Process operations with progressbar in the specified batch set.
   * 
   * @command batch_progressbar:process
   * @aliases batch-progressbar-process
   * @param $batch_id The batch id that will be processed.
   * @hidden
   * 
   * @return \Consolidation\OutputFormatters\StructuredData\UnstructuredListData
   */
  public function process($batch_id, $options = ['format' => 'json']) {
    \Drupal::moduleHandler()->loadInclude('drush_batch_progressbar', 'inc', 'includes/batch');
    return drush_progress_batch_command($batch_id);
  }
} 
