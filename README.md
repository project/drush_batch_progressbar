# Drush Batch Progressbar

Install this module and then progressbar can be called using:

```
    drush_backend_batch_process('batch-progressbar-process');
    // or
    drush_progressbar_batch_process();
```